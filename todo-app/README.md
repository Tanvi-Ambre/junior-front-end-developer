# todo-app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
cd todo-app
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### To run Playwright scripts
```
cd todo-app
Task 1 : npx playwright test tests/end-to-end.spec.ts
Task 2 : npx playwright test tests/task-list.spec.ts
```

### Future Enhancements
**To Do**

* Adding alerts and feedbacks for the user.
* Internationalization: Implement support for multiple languages to make the application accessible to a global audience.
* Improve Validation: Enhance the validation mechanisms to handle more complex scenarios and provide better user feedback.


**Covered Features**
* Accessibility: The application is designed to be accessible to users with disabilities, adhering to the best practices in web accessibility.


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
