import { test, expect, Page } from '@playwright/test';

test.describe('Clear All Tasks Feature', () => {
    test('Verify Clear All Tasks button functionality', async ({ page }: { page: Page }) => {

        // Step 1 : Navigate to the application
        try {
            await page.goto('http://localhost:8080/');
        } catch (error) {
            console.error('Failed to navigate to the application:', error instanceof Error ? error.message : error);
            throw new Error(`Navigation failed: ${error instanceof Error ? error.message : error}`);
        }

        // Step 2 : Add tasks
        try {
            await page.fill('[aria-label="New Task Input"]', 'Task 1');
            await page.click('[aria-label="Add Task appended action"]');
            await page.fill('[aria-label="New Task Input"]', 'Task 2');
            await page.click('[aria-label="Add Task appended action"]');
        } catch (error) {
            console.error('Failed to add tasks:', error instanceof Error ? error.message : error);
            throw new Error(`Task addition failed: ${error instanceof Error ? error.message : error}`);
        }

        // Step 3 : Take a screenshot before clearing tasks
        try {
            await page.screenshot({ path: 'before-clearing-tasks.png' });
        } catch (error) {
            console.error('Failed to take a screenshot before clearing tasks:', error instanceof Error ? error.message : error);
            throw new Error(`Screenshot failed: ${error instanceof Error ? error.message : error}`);
        }
        
        // Step 4 : Click on 'Clear All Tasks' button
        try {
            await page.click('[aria-label="Clear All Tasks"]');
        } catch (error) {
            console.error('Failed to click the Clear All Tasks button:', error instanceof Error ? error.message : error);
            throw new Error(`Clear All Tasks button click failed: ${error instanceof Error ? error.message : error}`);
        }

        // Step 5 : Verify that all task items are cleared from the list
        try {
            const tasksExist = await page.locator('.task-item').count();
            expect(tasksExist).toBe(0);
        } catch (error) {
            console.error('Verification failed:', error instanceof Error ? error.message : error);
            throw new Error(`Verification of task clearance failed: ${error instanceof Error ? error.message : error}`);
        }

        // Step 6 : Take a screenshot of the page after clearing the tasks
        try {
            await page.screenshot({ path: 'after-clearing-tasks.png' });
        } catch (error) {
            console.error('Failed to take a screenshot after clearing tasks:', error instanceof Error ? error.message : error);
            throw new Error(`Screenshot failed: ${error instanceof Error ? error.message : error}`);
        }
    });
});
