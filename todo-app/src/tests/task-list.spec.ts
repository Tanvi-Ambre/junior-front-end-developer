import { test, expect, Page } from '@playwright/test';

test.describe('TaskList Component Tests', () => {
    // Step 1 : Navigate to application page where TaskList is rendered
    test.beforeEach(async ({ page }: { page: Page }) => {
        await page.goto('http://localhost:8080/', { waitUntil: 'networkidle' }); 
    });

    // Step 2 : Ensure the add task input and add button are visible and interactable
    test('Should add a new task', async ({ page }: { page: Page }) => {
        await page.waitForSelector('[aria-label="New Task Input"]', { state: 'visible' });
        await page.fill('[aria-label="New Task Input"]', 'Complete Playwright setup');
        await page.waitForSelector('[aria-label="Add Task appended action"]', { state: 'visible' });
        await page.click('[aria-label="Add Task appended action"]');

        // Step 3 : Check if the task appears in the list
        await page.waitForSelector(':has-text("Complete Playwright setup")');
        const taskText: string | null = await page.textContent(':has-text("Complete Playwright setup")');
        expect(taskText).toContain('Complete Playwright setup');
    });

    // Step 4 : Add a new task titled "Go running" using the task input field
    test('Should mark a task as completed', async ({ page }: { page: Page }) => {
        await page.fill('[aria-label="New Task Input"]', 'Go running');
        await page.click('[aria-label="Add Task appended action"]');
    
        // Step 5 : Verify that the task "Go running" is added to the task list
        await page.waitForSelector('text="Go running"');
    
        // Step 6 : Find the checkbox associated with the task "Go running" and mark it
        const checkboxSelector = 'text="Go running" >> ../.. >> .v-selection-control__input >> input[type="checkbox"]';
        const checkbox = await page.waitForSelector(checkboxSelector);
        await checkbox.check();
    
        // Step 7 : Wait for the task to visually represent as completed by checking the text-decoration class
        const isCompleted: boolean = await page.evaluate((title: string) => {
            const taskTitles = Array.from(document.querySelectorAll('.v-list-item-title'));
            return taskTitles.some(el => el.textContent === title && el.classList.contains('text-decoration-line-through'));
        }, 'Go running');
    
        expect(isCompleted).toBeTruthy();
    });

    // Step 8 : Count the number of tasks
    test('Should update the list after task addition', async ({ page }: { page: Page }) => {
        await page.waitForSelector(':has-text("Task")');
        const initialCount: number = await page.locator(':has-text("Task")').count();

        // Step 9 : Add a new task and verify if its updated in existing list
        await page.fill('[aria-label="New Task Input"]', 'Read documentation');
        await page.click('[aria-label="Add Task appended action"]');
        await page.waitForSelector(':has-text("Read documentation")');
        
        const finalCount: number = await page.locator(':has-text("Task")').count();
        expect(finalCount).toBeGreaterThan(initialCount);
    });

    // Step 10 : Adding a task to ensure there's something to delete
    test('Should delete a task', async ({ page }: { page: Page }) => {
        await page.fill('[aria-label="New Task Input"]', 'Task to be deleted');
        await page.click('[aria-label="Add Task appended action"]');
        await page.waitForSelector('text="Task to be deleted"');
    
        // Deleting the task
        const deleteButtonSelector = 'text="Task to be deleted" >> ../.. >> button[aria-label="Delete task"]';
        await page.click(deleteButtonSelector);
        
        // Verify the task is no longer present
        const isVisible: boolean = await page.isVisible('text="Task to be deleted"');
        expect(isVisible).toBeFalsy();
    });
});
